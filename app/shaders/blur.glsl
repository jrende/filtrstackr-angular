<?xml version="1.0" encoding="UTF-8" ?>
<script id="shader-fs" type="x-shader/x-fragment" name="blur">
	precision mediump float;
	uniform vec2 res;
	uniform float amount;
	uniform float alpha;

	uniform sampler2D blurSampler;
	void main(void) {
	gl_FragColor = vec4(gl_FragColor.x, gl_FragColor.x, gl_FragColor.x, 1.0);
	}
</script>
