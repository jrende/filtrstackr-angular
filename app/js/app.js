'use strict';


// Declare app level module which depends on filters, and services
angular.module('filtrStackr', ['filtrStackr.filters', 'filtrStackr.services', 'filtrStackr.directives', 'filtrStackr.controllers']).
  config(['$routeProvider', function($routeProvider, availableFilters) {
    $routeProvider.when('/gauss/:filterId', {templateUrl: 'partials/gauss.html', controller: 'FilterSettingsCtrl'});
    $routeProvider.when('/noise/:filterId', {templateUrl: 'partials/noise.html', controller: 'FilterSettingsCtrl'});
    $routeProvider.when('/clouds/:filterId', {templateUrl: 'partials/clouds.html', controller: 'FilterSettingsCtrl'});
    $routeProvider.when('/canvas/:filterId', {templateUrl: 'partials/canvas.html', controller: 'FilterSettingsCtrl'});
    $routeProvider.when('/blur/:filterId', {templateUrl: 'partials/blur.html', controller: 'FilterSettingsCtrl'});
    $routeProvider.when('/flat/:filterId', {templateUrl: 'partials/flat.html', controller: 'FilterSettingsCtrl'});
    $routeProvider.when('/fallthrough/:filterId', {templateUrl: 'partials/fallthrough.html', controller: 'FilterSettingsCtrl'});
    $routeProvider.when('/newFilter', {templateUrl: 'partials/newfilter.html', controller: 'NewFilterCtrl'});
  }]);
