'use strict';

/* Directives */


var dirs = angular.module('filtrStackr.directives', []);
dirs.directive('appVersion', ['version', function(version) {
	return function(scope, elm, attrs) {
		elm.text(version);
	};
}]);
dirs.directive('sortable', function(currentFilters, renderer) {
		return {
			restrict: 'A',
			link: function(scope, elm, attrs) {
				var elem = $(elm);
				console.log("Make sortable accordion list");
				elem.sortable({
					axis: 'y',
					items: "li:not(.canvas)",
				});
				elem.on("sortdeactivate", function(event, ui) {
					var targetList = [];
					elem.children().each(function(index, element) {
						var id = element.getAttribute("id");
						if(id !== null) {
							targetList.push(Number(id.split(/filter-/)[1]));
						}
					});
					if(targetList.length !== currentFilters.list.length) {
						console.log("Targetlist not equal to filter length!!");
					}
					var newList = [];	
					for(var i = 0; i < targetList.length; i++) {
						newList[i] = currentFilters.list[targetList[i]]
					}
					angular.copy(newList, currentFilters.list);
					scope.$digest();
					renderer.drawImage();
				});
			}
		}
});
dirs.directive('webglCanvas', function(renderer) {
        return {
                restrict: 'A',
                link: function(scope, elm, attrs) {
                        renderer.init();
                }
        }
});

