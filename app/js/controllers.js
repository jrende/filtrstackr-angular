'use strict';

/* Controllers */

angular.module('filtrStackr.controllers', []).
controller('FiltrStackrMain', function($scope, currentFilters, $location, renderer, storage) {
	$scope.filterList = currentFilters.list;
	$scope.showView = false;
	$scope.canvas = currentFilters.list[0];

	$scope.filterClick = function($event, filter) {
		if(filter === "new") {
			$location.path("#/newFilter");
		} else {
			$location.path("/" + filter.type.name + "/" + filter.id);
		}
		$scope.showView = true;
	}

	$scope.removeFilter = function($event, filter) {
		$event.stopPropagation();
		currentFilters.removeFilterById(filter.id);
		renderer.drawImage();
	}

	$scope.closeView = function() {
		$scope.showView = false;
		$location.path("/");
	}

	$scope.$on('$viewContentLoaded', function() {
		if($location.path() === "/") {
			$scope.showView = false;
		}
	});
	$scope.loadFilters = function() {
			storage.loadState();
	}
	$scope.saveFilters = function() {
		storage.saveState();
	}
	$scope.clearFilters = function() {
		currentFilters.clearFilters();
	}
	$scope.print = function() {
		console.log("hej");
	}

})
.controller('FilterSettingsCtrl', function($scope, currentFilters, $routeParams, renderer, availableFilters) {
	var selectedFilter = currentFilters.getFilterById($routeParams.filterId);
	$scope.filter = selectedFilter;

	$scope.blendModes = availableFilters.blendModes;

	$scope.setFilter = function() {
		var params = selectedFilter.type.parameters;
		for(var i = 0; i < params.length; i++) {
			selectedFilter[params[i]] = Number($scope.filter[params[i]]);
		}
		selectedFilter.blendMode = $scope.filter.blendMode;
		renderer.drawImage();
	};

})
.controller('NewFilterCtrl', function($scope, availableFilters, currentFilters, renderer) {
	$scope.filters = availableFilters;
	//Maybe some validation here
	$scope.addNewFilter = function() {
		var filter = {
			"type": availableFilters.getAvailableFilterByName($scope.newFilterType)
		};
		currentFilters.addNewFilter(filter);
		renderer.drawImage();
	}

});
