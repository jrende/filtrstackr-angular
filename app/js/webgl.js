var Renderer = function(availableFilters) {
	var gl;
    function initGL(canvas) {
        try {
            gl = canvas.getContext("experimental-webgl");
            gl.viewportWidth = canvas.width;
            gl.viewportHeight = canvas.height;
        } catch (e) {
        }
        if (!gl) {
            alert("Could not initialise WebGL, sorry :-(");
        }
    }

    function getShader(gl, shaderSrc) {
        var shader;
        if (shaderSrc.type == "x-shader/x-fragment") {
            shader = gl.createShader(gl.FRAGMENT_SHADER);
        } else if (shaderSrc.type == "x-shader/x-vertex") {
            shader = gl.createShader(gl.VERTEX_SHADER);
        } else {
            return null;
        }

        gl.shaderSource(shader, shaderSrc.src);
        gl.compileShader(shader);

        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            alert(gl.getShaderInfoLog(shader));
            return null;
        }

        return shader;
    }

	var vertexShader = {
	      "src" : "attribute vec3 aVertexPosition;\n" + 
		  "void main(void) {\n" +
			"gl_Position = vec4(aVertexPosition, 1.0);\n" +
		  "}",
		  "type": "x-shader/x-vertex"
	}

	function initShaders(shaderSources) {
		var shaderList = {};
        vertexShader = getShader(gl, vertexShader);
		for(var i = 0; i < shaderSources.length; i++) {
			var shaderProgram = createShaderProgram(shaderSources[i]);
			shaderList[shaderSources[i].name] = shaderProgram;
		}
		return shaderList;
	}
	
	var texFBO;
	var texture;
	function initTexture() {
		texFBO = gl.createFramebuffer();
		gl.bindFramebuffer(gl.FRAMEBUFFER, texFBO);
		texFBO.width = 512;
		texFBO.height = 512;
		
		texture = gl.createTexture();
		gl.bindTexture(gl.TEXTURE_2D, texture);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, texFBO.width, texFBO.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);

		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.generateMipmap(gl.TEXTURE_2D);


		gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0);

		gl.bindTexture(gl.TEXTURE_2D, null);
		gl.bindRenderbuffer(gl.RENDERBUFFER, null);
		gl.bindFramebuffer(gl.FRAMEBUFFER, null);

		/*
		texture = gl.createTexture();
		texture.image = new Image();
		texture.image.onload = function() {
			gl.bindTexture(gl.TEXTURE_2D, texture);
			//gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.bindTexture(gl.TEXTURE_2D, null);
		}
		texture.image.src = "/app/img/rock.png";

		*/
	}

    function createShaderProgram(shaderSource) {
        var fragmentShader = getShader(gl, shaderSource);

        var shaderProgram = gl.createProgram();
        gl.attachShader(shaderProgram, vertexShader);
        gl.attachShader(shaderProgram, fragmentShader);
        gl.linkProgram(shaderProgram);

        if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
            alert("Could not initialise shaders");
        }

        gl.useProgram(shaderProgram);

        shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
        gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

		shaderProgram.uniforms = {};
		filterType = availableFilters.getAvailableFilterByName(shaderSource.name);
		for(var i = 0; i < filterType.parameters.length; i++) {
			shaderProgram.uniforms[filterType.parameters[i]] = gl.getUniformLocation(shaderProgram, filterType.parameters[i]);
		}
		shaderProgram.uniforms["tSampler"] = gl.getUniformLocation(shaderProgram, "tSampler");
		shaderProgram.uniforms["res"] = gl.getUniformLocation(shaderProgram, "res");

		gl.bindBuffer(gl.ARRAY_BUFFER, squareVertexPositionBuffer);
		gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, squareVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);
		return shaderProgram;
    }

    var squareVertexPositionBuffer;
    function initBuffers() {
        squareVertexPositionBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, squareVertexPositionBuffer);
        vertices = [
             1.0, 1.0, 0.0,
            -1.0, 1.0, 0.0,
             1.0, -1.0, 0.0,
            -1.0, -1.0, 0.0
        ];
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
        squareVertexPositionBuffer.itemSize = 3;
        squareVertexPositionBuffer.numItems = 4;

    }

	var ready = 0;
	function loadShaders(callback) {
		var list = availableFilters.list;
		var shaderSources = [];
		var done = 0;
		for(var i = 0; i < list.length; i++) {
			jQuery.get("shaders/" + list[i].name + ".glsl", function(data, textstatus, jqXHR) {
				var p = $(jQuery.parseXML(data).documentElement);		
				var type = p.attr("type");
				var name = p.attr("name");
				var src = p.text();
				shaderSources.push({"type": type, "src": src, "name": name});
				done++;
				console.log(done + " done.");
				if(done === (list.length)) {
					ready = 1;
					callback(shaderSources);
				}
			});
		}
	}

	var shaderList;
	this.initCanvas = function(callback) {
		var canvas = document.getElementById("webglCanvas");
		initGL(canvas);
		initBuffers();
		initTexture();
		loadShaders(function(shaderSources) {
			shaderList = initShaders(shaderSources);
			callback();
		});
		gl.clearColor(0.0, 0.0, 0.0, 1.0);
		gl.disable(gl.DEPTH_TEST);
		gl.enable(gl.BLEND);
		//gl.blendEquation(FUNC_ADD, FUNC_ADD);
	}

    this.drawScene = function(filters) {
		if(ready) {
			gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
			gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT | gl.STENCIL_BUFFER_BIT);
			for(var i = 0; i < filters.length; i++) {
				var filter = filters[i];
				var shader = shaderList[filter.type.name];
				if(shader != null) {
					gl.useProgram(shader);
					gl.uniform2fv(shader.uniforms["res"], [gl.viewportWidth, gl.viewportHeight]);
					for(var j = 0; j < filter.type.parameters.length; j++) {
						var parameter = filter.type.parameters[j];
						var argument = filter[parameter];
						gl.uniform1f(shader.uniforms[parameter], argument);
					}
					if(i > 0) {
						gl.activeTexture(gl.TEXTURE0);
						gl.bindTexture(gl.TEXTURE_2D, texture);
						gl.uniform1i(shader.uniforms["tSampler"], 0);
					}
					
					//Rita till skärmen, med det förra filtrets textur
					gl.blendFunc(gl[filter.blendMode.srcFactor], gl[filter.blendMode.dstFactor]);
					gl.drawArrays(gl.TRIANGLE_STRIP, 0, squareVertexPositionBuffer.numItems);


					//Rita om filtret till texturen.
					//Funkar ISH! verkar skriva över det tidigare resultatet... hitta någon mer funktionell metod.
					gl.bindFramebuffer(gl.FRAMEBUFFER, texFBO);
					gl.blendFunc(gl[filter.blendMode.srcFactor], gl[filter.blendMode.dstFactor]);
					gl.drawArrays(gl.TRIANGLE_STRIP, 0, squareVertexPositionBuffer.numItems);

					gl.bindFramebuffer(gl.FRAMEBUFFER, null);
				}
			}
		}
    }
}
