'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
var fsModule = angular.module('filtrStackr.services', []);
fsModule.value('version', 0.1);
fsModule.factory('currentFilters', function() {
	var nextId = 1
	return {
		"list": [
			{
				"id": 0,
				"type": {
					"visibleName": "Canvas settings", 
					"name": "canvas",
				},
				"width": 512,
				"height": 512,
			}
		],
		"getFilterById": function(id) {
			for(var i = 0; i < this.list.length; i++) {
				//One or two equals?
				if(this.list[i].id == id) {
					return this.list[i];
				}
			}
			return null;
		},
		"removeFilterById": function(id) {
			//The first "filter" is always the canvas settings.
			for(var i = 1; i < this.list.length; i++) {
				//One or two equals?
				if(this.list[i].id == id) {
					this.list.splice(i, 1);
					return;
				}
			}
		},
		"addNewFilter": function(filter) {
			filter.id = nextId++;
			initFilter(filter);
			this.list.push(filter);
		}, 
		"clearFilters": function() {
			var canvas = this.list[0];	
			angular.copy([canvas], this.list);
		}
	}
});
function initFilter (filter) {
	var injector = angular.injector(['filtrStackr.services', 'ng']);
	var availableFilters = injector.get('availableFilters');

	if(filter != null) {
		filter.alpha = 1.0;
		filter.blendMode = availableFilters.blendModes[0];
	}
	switch(filter.type.name) {
		case "clouds":
			filter.seed = Number((Math.random()*(100000)).toFixed());
			filter.size = 1;
			break;
		case "blur":
			filter.amount = 1;
			break;
		case "flat":
			filter.r = 1.0;
			filter.g = 1.0;
			filter.b = 1.0;
			break;
		case "gauss":
			break;
		case "fallthrough":
			break;
		case "noise":
			break;
		default:
			throw new UnknownFilterException("Unknown filter name: " + filter.type.name);
	}
}
function UnknownFilterException (message) {
	this.message = message;
	this.name = "FilterException";
}
UnknownFilterException.prototype.toString = function() {
	return this.name + ': "' + this.message + '"';
}

fsModule.factory('availableFilters', function() {
	return {
		"list": [
				{
					"visibleName": "Gaussian blur", 
					"name": "gauss",
					"parameters" : [
						"alpha",
						"radius"
					]
				},
				{
					"visibleName": "Fall Through",
					"name": "fallthrough",
					"parameters" : [
						"alpha"
					]
				},
				{
					"visibleName": "Clouds",
					"name": "clouds",
					"parameters" : [
						"alpha",
						"seed",
						"size",
						"depth",
						"nabla"
					]
				},
				{
					"visibleName": "Flat color",
					"name": "flat",
					"parameters" : [
						"alpha",
						"r",
						"g",
						"b"
					]
				},
				{
					"visibleName": "Noise",
					"name": "noise",
					"parameters" : [
						"alpha",
						"amount"
					]
				},
				{
					"visibleName": "Blur",
					"name": "blur",
					"parameters" : [
						"alpha",
						"amount"
					]
				}
		],
		"blendModes": [
			{
				"visibleName": "Normal",
				"srcFactor": "SRC_ALPHA",
				"dstFactor": "ONE_MINUS_SRC_ALPHA"
					
			},
			{
				"visibleName": "Multiply",
				"srcFactor": "DST_COLOR",
				"dstFactor": "ONE_MINUS_SRC_ALPHA"
			},
			{
				"visibleName": "Screen",
				"srcFactor": "SRC_ALPHA",
				"dstFactor": "ONE_MINUS_SRC_COLOR"
			},
		],
		"getAvailableFilterByName": function(name) {
			for(var i = 0; i < this.list.length; i++) {
				if(this.list[i].name === name) {
					return this.list[i];
				}
			}
			return null;
		}
	}
});

fsModule.factory('renderer', function(availableFilters, currentFilters) {
	var renderer = new Renderer(availableFilters);
	return {
		"init": function() {
			renderer.initCanvas(function() {
				renderer.drawScene(currentFilters.list.slice(1));
			});
		},
		"drawImage": function() {
			renderer.drawScene(currentFilters.list.slice(1));
		}
		
	}
});

fsModule.factory('storage', function(currentFilters) {

	return {
		"saveState": function() {
			localStorage.setItem("FiltrStackr-state", JSON.stringify(currentFilters.list));
		}, 
		"loadState": function() {
			var list = localStorage.getItem("FiltrStackr-state");
			if(list != null) {
				//currentFilters.list = JSON.parse(list);
				angular.copy(JSON.parse(list), currentFilters.list)
			}
		}
	}
});

